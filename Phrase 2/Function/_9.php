<?php
/**
 *   Create a function to create a new array of given length using the odd numbers from a given array of positive integers
 * Sample Input:
{1,2,3,5,7,9,10},3
Expected Output:
New array: 1,3,5
 */

function oddArray($arr,$n){
    $oddArr = [];
    for ($j=0;$j<count($arr);$j++){
        if ($arr[$j]%2==1 && count($oddArr)<$n){
            $oddArr[] = $arr[$j];
        }
    }

    return $oddArr;
}

$arr = [1,2,3,5,7,9,10];
print_r(oddArray($arr,3));  //[1,3,5]