<?php
/**
 * What is a "function" in PHP?
 */

/**
 * A function is a block of code to resolve a problem and can be used multiple times in a program
 *
 * A function will not be loaded automatically but need to be called to be used.
 */