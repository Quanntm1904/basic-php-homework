<?php
/**
 *   Create a function to check a given array of integers and return true if the given array contains two 5's next to each other, or two 5 separated by one element
 * Sample Input:
{ 5, 5, 1, 5, 5 }
{ 1, 2, 3, 4 }
{ 3, 3, 5, 5, 5, 5}
{ 1, 5, 5, 7, 8, 10}
Expected Output:
true
false
true
true
 */

function fiveCheck($arr){
    $subarr = [5,5];
    if (array_count_values($arr)['5']<4||!in_array($subarr,$arr)){
        return false;
    }else{
        return true;
    }
}

$arr = [5, 5, 1, 5, 5 ];

if (fiveCheck($arr)) echo 'True'; else echo 'False';
