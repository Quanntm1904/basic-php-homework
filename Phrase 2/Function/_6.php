<?php
/**
 *  Create a function that checks whether a string variable is all lowercase
 */

function isLowercase($str){
    for ($i=0;$i<strlen($str);$i++){
        if (!ctype_lower($str[$i])){
            return false;
        }
    }
    return true;
}

$str = 'vcicuiwcbicio';
if (isLowercase($str)){
    echo "All lowercase";
}
else echo "Not all lowercase";
