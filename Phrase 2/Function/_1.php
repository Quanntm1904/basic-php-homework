<?php
/**
 *   Create a script to get start and end date of a week (by week number) of a particular year
 *   Sample week and year : 10, 2020
 *   Expected Result : Starting date of the week: xx-xx-xx - End date the week: xx-xx-xx
 */

$format='d-m-Y';


function getDateWeek($week, $year) {
    global $format;
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['start'] = $dto->format($format);
    $dto->modify('+6 days');
    $ret['end'] = $dto->format($format);
    return $ret;
}
$week = getDateWeek(10,2019);
echo "Start at ". $week['start']. "<br> End at " . $week['end'];