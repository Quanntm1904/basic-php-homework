<?php
/**
 *  Create a function to sort a numeric array
 */


/**
 * @param $arr=[15,4,7,3,4,32,87,33,34,12];
 */

function sortArr($arr){
    for ($i=0;$i<count($arr);$i++){
        for ($j = $i;$j<count($arr);$j++){
            if ($arr[$j]<$arr[$i]){
                $c = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $c;
                
                // hàm return thì k được echo/print nữa.
                // trong trường hợp debug thì sau khi xong phải xóa đi.
                
                print_r($arr);
                echo "<br>";
            }
        }
    }
    return $arr;
}

$arr=[15,4,7,3,4,32,87,33,34,12];

print_r(sortArr($arr)); // [3,4,4,7,12,15,32,33,34,87];
