<?php
/**
 *  Create a function to compute the sum of the two given integer values. If the two values are the same, then returns triple their sum
 *  Sample Input
1, 2
3, 2
2, 2
Expected result:
3
5
12
 */

function sumFun($a,$b){
    if ($a!=$b){
        return $a+$b;
    }else return 3*($a+$b);
}

echo sumFun(1,2).'<br>';
echo sumFun(3,2).'<br>';
echo sumFun(2,2).'<br>';

/**
3
5
12
 */
