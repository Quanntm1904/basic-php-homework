<?php
/**
 *  Write an array of months and their corresponding number of days, with a function to create option elements for a select field
 */
 
$year = [
    'January'=>31,
    'February'=>28,
    'March'=>31,
    'April'=>30,
    'May'=>31,
    'June'=>30,
    'July'=>31,
    'August'=>31,
    'September'=>30,
    'October'=>31,
    'November'=>30,
    'December'=>31
];

// bài này đang thiếu hàm tạo select options html
