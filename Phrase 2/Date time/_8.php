<?php
/**
 *   Create a script to convert a date from yyyy-mm-dd to dd-mm-yyyy
 */

function converter($time){

    $format = 'd-m-Y';
    $var = strtotime($time);
    $time = date($format,$var);
    return $time;
}

echo converter('2020-4-19'); // 19-04-2020