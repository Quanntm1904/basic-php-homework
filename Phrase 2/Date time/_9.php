<?php
/**
 *   Create a function to calculate the factorial of a number (a non-negative integer). The function accepts the number as an argument
 */

function factorial($n){
    if (n<0){
        return false;
    } elseif ($n==0|| $n == 1){
        return 1;
    }else{
        return $n*factorial($n-1);
    }
}

echo factorial(4); //24
