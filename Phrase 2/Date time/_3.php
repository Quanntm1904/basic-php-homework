<?php
/**
 * Create a script to determine time 29 days ago
 */

$format = "d-m-Y H:i:s";

$time =  strtotime("- 29 days");

echo date($format,$time);
