<?php
/**
 *  2. Viết chương trình tính diện tích hình chữ nhật
 *  Input :
 *         + Chiều dài
 *         + Chiều rộng
 *  Output:
 *         + Diện tích
 *
 */

function areaCal($length, $width){
    return $length*$width;
}

echo areaCal(5, 10);    //50
