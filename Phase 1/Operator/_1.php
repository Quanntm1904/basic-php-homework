<?php
/**
 *  1. Viết chương trình tính tuổi của tôi
 *  Input :
 *         + Năm sinh
 *         + Năm hiện tại
 *  Output:
 *         + Tuổi hiện tại
 *
 *  Algorithm :
 *           . Tuổi = năm hiện tại - năm sinh
 *           -> sử dụng hàm date('Y') để lấy năm hiện tại
 */
function myAge($birth)
{
    $current = date('Y');
    $age = $current - $birth;
    return $age;
}

echo "You are " . myAge(1998) . " years old";   //You are 22 years old