<?php
/**
 *  4. Tính tổng số nguyên chẵn tử 1 -> n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */
function calculate($n){
    $i = 1;
    $S = 0;
    while ($i<=$n ){
        if ($i%2==0) {
            $S += $i;
        }
        $i+=1;
    }
    return $S;
}

echo calculate(4);     //6