<?php
/***
 *  6. Kiểm tra một số có phải là số nguyên tố hay không (sử dụng vòng lặp while)
 *  Input :
 *         + Khai báo number
 *  Output:
 *         + Xác định $number có phải là số nguyên tố hay không ?
 *
 */

function isPrime($n){
    $i = 2;
    while ($i<$n){
        if ($n%$i!=0){
            $i++;

        }else {
            return false;
        }
    }
    return true;
}

$n=33;

if (isPrime($n)){
    echo "$n is a prime number";
}else echo "$n is not a prime number";  //33 is not a prime number