<?php
/**
 *  3. Tính tổng số nguyên lẻ tử 1 -> n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */

function calculate($n){
    $i = 1;
    $S = 0;
    while ($i<=$n ){
        if ($i%2==1) {
            $S += $i;
        }
        $i+=1;
    }
    return $S;
}

echo calculate(4);  //4