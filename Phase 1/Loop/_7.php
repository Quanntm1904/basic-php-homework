<?php
/**
 *  7. In ra tất cả số nguyên tố nhỏ hơn n (sử dụng vòng lặp for)
 *  Input :
 *         + Khai báo number
 *  Output:
 *         + Xác định $number có phải là số nguyên tố hay không ?
 *
 */

function isPrime($n){
    $i = 2;
    while ($i<$n){
        if ($n%$i!=0){
            $i++;

        }else {
            return false;
        }
    }
    return true;
}

function primeList($n){
    for ($i=1;$i<=$n;$i++){
        if (isPrime($i)) echo "$i; ";
    }
}

primeList(10);  //1; 2; 3; 5; 7;