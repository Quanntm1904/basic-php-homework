<?php
/**
 *  5. Bài tập tính S = 1! + 2! + 3! + ... + n! (sử dụng vòng lặp while)
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */
function calculate($n){
    $i = 1;
    $S = 0;
    $mul=1;
    while ($i<=$n){
        $mul *=$i;
        $S+=$mul;
        $i++;
    }

    return $S;
}

echo calculate(4);  //33