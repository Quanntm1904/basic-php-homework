<?php
/**
 *  1. Bài tập tính tổng : S = 1 + 2 + 3 + 4 + ... + n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 */
function calculate($n){
    $i = 1;
    $S = 0;
    while ($i<=$n){
        $S+=$i;
        $i++;
    }
    return $S;
}

echo calculate(4);  //10