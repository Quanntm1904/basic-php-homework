<?php
/**
 *  2. Tính tổng : S = 1 + 1*2 + 1*2*3 + 1*2*3*4 + ... + 1*2*3*4*...*n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */

function calculate($n){
    $S = 0;
    $mul=1;

    for ($i=1;$i<=$n;$i++){
        $mul *=$i;
        $S+=$mul;
    }
    return $S;
}

echo calculate(4);  //33