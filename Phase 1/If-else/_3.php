<?php
/**
 *  3. Bài toán tính tiền taxi với số km cho trước
 *  Input :
 *         + 1km đầu giá = 15000 đ
 *         + từ 1km đến 5km giá = 12000 đ
 *         + từ 6km trở đi giá  = 90000 đ
 *         + từ 140km trở đi được giảm 12 % tổng tiền
 *  Output:
 *         + Số tiền cần thanh toán
 */

function taxiFee($dis){

    if ($dis<=1){
        $fee=15000;
    } elseif ($dis<=5){
        $fee = 15000 + ($dis-1)*12000;
    }else{
        $fee = ($dis-5)*90000 + 63000;
    }
    if ($dis > 140){
        $fee*=0.88;
    }

    return $fee;
}

$dis = 1.9;

echo taxiFee($dis);     //25800