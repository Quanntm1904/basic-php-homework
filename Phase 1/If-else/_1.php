<?php
/**
 *  1. Viết chương trình so sánh 2 số
 *  Input :
 *         + firstNumber = 2;
 *         + secondNumber = 9;
 *  Output:
 *         + secondNumber lớn hơn firstNumber
 *
 *  Algorithm :
 *           ...
 *
 */

function compare($a,$b){
    if ($a<$b){
        echo $a ." < ".$b;
    }elseif ($a>$b){
        echo $a." > ".$b;
    }else echo "They are equal";
}

compare(2, 9); //2 < 9