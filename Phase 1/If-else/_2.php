<?php
/**
 *  2. Viết chương trình tìm max của 3 số
 *  Input :
 *         + firstNumber   = 2;
 *         + secondNumber   = 9;
 *         + thirdNumber = 4;
 *  Output:
 *         + Max là secondNumber
 */

function findMax($a, $b, $c)
{
    $max = $a;
    if ($b > $max) {
        $max = $b;
    }
    if ($c > $max) {
        $max = $c;
    }

    return $max;
}

echo "Max number is ". findMax(2, 9, 4);    //Max number is 9