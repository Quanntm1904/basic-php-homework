<?php
/**
 *  2. CRUD Array (Thêm sửa xóa phần tử của mảng )
 *
 */

$products = [
    [
        'name' => 'Bphone-2019',
        'price' => 1000000,
    ]
];


$new_product =  [
    'name' => 'Vsmart-Live',
    'price' => 2000000,
];

array_push($products,$new_product);
print_r($products);

/*
 * Array ( [0] => Array ( [name] => Bphone-2019 [price] => 1000000 ) [1] => Array ( [name] => Vsmart-Live [price] => 2000000 ) )
 * */