<?php
/**
 * 4. Thêm một phần tử vào vị trí bất kì của mảng
 *
 */

$number = [1, 2, 3, 4, 5];

$pos = 3; // Vị trí cần thêm

$val = 22; // Giá trị cần thêm
echo "Before: ";
foreach ($number as $i) {
    echo "$i ";
}
echo "<br>";

array_splice($number, $pos, 0, $val);
echo "After: ";
foreach ($number as $i) {
    echo "$i ";
}

/*
    Before: 1 2 3 4 5
    After: 1 2 3 22 4 5
*/