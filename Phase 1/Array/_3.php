<?php
/**
 *  3. Tìm kiếm phần tử Vsmart-Live trong mảng
 */

$products = [
    [
        'name' => 'Bphone-2019',
        'price' => 1000000,
    ],
    [
        'name' => 'Vsmart-Live',
        'price' => 3790000,
    ],
    [
        'name' => 'Vsmart-Active',
        'price' => 4890000,
    ],
];

foreach ($products as $index => $product){
    if ($product['name']=='Vsmart-Live'){
        echo $product['price'];                 //3790000
        echo "<br> Position: ". ($index +1) ;   //Position: 2
    }
}

/**
 * 3.1 Hiển thị phần tử đầu tiên của mảng
 */
echo "<br>";
print_r($products[0]);                          //Array ( [name] => Bphone-2019 [price] => 1000000 )