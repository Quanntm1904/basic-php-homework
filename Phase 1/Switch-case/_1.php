<?php
/**
 *  1. Bài toán xây dựng câu hỏi thi trắc nghiệm
 *  Input :
 *         + Khai báo yourAnswer
 *  Output:
 *         + Thông báo kết quả của câu hỏi
 */

echo('Câu hỏi : 3 * 7 = ? <br />' );
echo('a. 21 <br />' );
echo('b. 18 <br />' );
echo('c. 4 <br />' );
echo('d. 29 <br />' );

$yourAnswer =  'a';

switch ($yourAnswer){
    case 'a':
        echo "Right answer!";
        break;
    default:
        echo "Wrong answer, please try again";
}
/**
 * Câu hỏi : 3 * 7 = ?
a. 21
b. 18
c. 4
d. 29
Right answer!
 */