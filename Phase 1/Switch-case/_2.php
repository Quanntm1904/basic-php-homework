<?php
/**
 *  2. Bài toán xây dựng máy tím bỏ túi đơn giản
 *  Input :
 *         + Khai báo firstNumber và secondNumber
 *         + Khai báo phép toán  (+ - * / %)
 *  Output:
 *         + Kết quả của biểu thức
 */

$firstNumber  = 4;
$secondNumber = 29;
$operator     = '*';

switch ($operator){
    case '+':
        echo "$firstNumber + $secondNumber = " . ($firstNumber+$secondNumber);
        break;
    case '-':
        echo "$firstNumber - $secondNumber = " . ($firstNumber-$secondNumber);
        break;
    case '*':
        echo "$firstNumber * $secondNumber = " . ($firstNumber*$secondNumber);
        break;
    case '/':
        echo "$firstNumber / $secondNumber = " . ($firstNumber/$secondNumber);
        break;
    case '%':
        echo "$firstNumber % $secondNumber = " . ($firstNumber%$secondNumber);
        break;
}

//4 * 29 = 116